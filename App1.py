# -*- coding: utf-8 -*-
"""
Created on Thu Apr 20 11:26:27 2017

@author: Mihai Darie
"""
import numpy as np
import tkinter as tk
import webbrowser as wb

master = tk.Tk()

s=1
d=1

def callback():
    file = open('Readme1.txt')
    print (file.read())
    wb.open('Readme1.txt')

def width_val(wid):
    canvas.delete("all")
    wid = varWidth.get()
    dist = varDist.get()
    canvas.create_line(550+dist, 20, 550+dist, 580, width=wid)
    canvas.create_line(550-dist, 20, 550-dist, 580, width=wid)
    canvas.update()

def dist_val(dist):
    canvas.delete("all")
    wid=varWidth.get()
    dist = varDist.get()
    canvas.create_line(550+dist, 20, 550+dist, 580, width=wid)
    canvas.create_line(550-dist, 20, 550-dist, 580, width=wid)
    canvas.update()

def refresh():
    d = int(sb.get())
    s=scale_1.get()/2#divided by 2 to find the distance on my screen
    a=int(s/2*d)
    v=2*np.arctan(a)
    #canvas.delete(tk.ALL)
    #canvas.create_line(550+scale_1.get() , 20, 550+scale_1.get() , 580, width=scale_2.get())
    #canvas.create_line(550-scale_1.get(), 20, 550-scale_1.get(), 580, width=scale_2.get())
    print(v*v/10)
    sb.insert(0, v*v/10)




#d=1
#s=1
#a=s/2*d

#v = 2*arctan()

label_1 = tk.Label(master, text="Width:")
label_1.grid(row=0, sticky=tk.E)

label_2 = tk.Label(master, text="Distance:")
label_2.grid(row=1, sticky=tk.E)

label_3 = tk.Label(master, text="Enter value & calculate")
label_3.grid(row=0, column=4, sticky=tk.N)

label_3 = tk.Label(master, text="your angular vision")
label_3.grid(row=0, column=4, sticky=tk.S)

varWidth = tk.IntVar()

scale_1 = tk.Scale(master, from_=0, to=200, orient=tk.HORIZONTAL, length=700, variable = varWidth, command=width_val)
scale_1.grid(row=0, column=1, sticky=tk.W)

varDist = tk.IntVar()

scale_2 = tk.Scale(master, from_=0, to=400, orient=tk.HORIZONTAL, length=700, variable = varDist, command=dist_val)
scale_2.grid(row=1, column=1, sticky=tk.W)

canvas = tk.Canvas(master, width=1100, height=600, background='white')
canvas.grid(row=3,columnspan=2)

#canvas.coords(line1, scale_1.get() , 20, scale_1.get() , 80)
#canvas.update_idletasks

line1 = canvas.create_line(550, 20, 550, 580, width=1)
line2 = canvas.create_line(550, 20, 550, 580, width=1)

#canvas.delete()
#canvas.coords(line1, scale_1.get() , 20, scale_1.get() , 80)
#canvas.update

button1 = tk.Button(master, text="Readme", command = callback)
button2 = tk.Button(master, text="Calculate", command = refresh)

button1.grid(row=3, column=4, sticky = tk.S)
button2.grid(row=3, column=4, sticky = tk.N)

sb = tk.Spinbox(master, from_=1, to=600, width=10)
sb.grid(row=1, column=4)

#s = int(sb.get())
#c=scale_1.get()

master.geometry('1250x720')
master.mainloop()