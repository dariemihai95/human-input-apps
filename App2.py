# -*- coding: utf-8 -*-
"""
Created on Sun Apr 23 14:50:53 2017

@author: Mihai Darie
"""
import tkinter as tk
import pyaudio
import numpy as np
import matplotlib.pyplot as plt
import webbrowser as wb

master = tk.Tk()
p = pyaudio.PyAudio()
fs = 44100
i=0
tries = [0]
myList = [0]

def callback():
    file = open('Readme2.txt', 'r')
    print (file.read())
    wb.open('Readme2.txt')

def play():
    volume = scale_1.get()/100 # range [0.0, 1.0]
    duration = scale_2.get() # in seconds, may be float
    f = scale_3.get() # sine frequency, Hz, may be float
    samples = (np.sin(2*np.pi*np.arange(fs*duration)*f/fs)).astype(np.float32)
    stream = p.open(format=pyaudio.paFloat32, channels=1, rate=fs, output=True)
    stream.write(volume*samples)
    #myList.extend([np.log(f)])

def record():
    f = scale_3.get()
    global i
    i += 1
    tries.extend([i])
    myList.extend([np.log(f)])
    
    
def plot():
    plt.figure(figsize=(10,5)) #increased the size so it can be seen better
    plt.title('Hearing test')
    plt.xlabel('Tries')
    plt.ylabel('Frequency')
    plt.plot(tries,myList,'o-')
    plt.show()

label_1 = tk.Label(master, text="Volume:")
label_1.grid(row=0)

label_2 = tk.Label(master, text="Duration:")
label_2.grid(row=1)

label_3 = tk.Label(master, text="Frequency:")
label_3.grid(row=2)

scale_1 = tk.Scale(master, from_=0, to=100, orient=tk.HORIZONTAL, length=500)
scale_1.grid(row=0, column=1)

scale_2 = tk.Scale(master, from_=0, to=60, orient=tk.HORIZONTAL, length=500)
scale_2.grid(row=1, column=1)

scale_3 = tk.Scale(master, from_=20, to=20000, orient=tk.HORIZONTAL, length=500)
scale_3.grid(row=2, column=1)

button1 = tk.Button(master, text="Play", command = play)
button1.grid(row=0, column=2, sticky = tk.S)

button2 = tk.Button(master, text="Readme", command = callback)
button2.grid(row=4, column=1, sticky = tk.S)

button3 = tk.Button(master, text="Record", command = record)
button3.grid(row=1, column=2, sticky = tk.S)

button4 = tk.Button(master, text="Plot", command = plot)
button4.grid(row=2, column=2, sticky = tk.S)

master.geometry('650x200')
master.mainloop()