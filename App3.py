# -*- coding: utf-8 -*-
"""
Created on Sun Apr 23 16:34:12 2017

@author: Mihai Darie
"""
import tkinter as tk
import numpy as np
import webbrowser as wb

master = tk.Tk()


canvas1 = tk.Canvas(master, width=700, height=700, background='white')
canvas1.grid(row=0, column=1)
canvas = tk.Canvas(master, width=650, height=700, background='black')
canvas.grid(row=0, column=3)

def callback():
    file = open('Readme3.txt')
    print (file.read())
    wb.open('Readme3.txt')

def change(col):
    global canvas
    col = varColor.get()
    canvas.delete("all")
    #print(col)
    color = ('gray'+np.str(col))
    canvas = tk.Canvas(master, width=650, height=700, background=color)
    canvas.grid(row=0, column=3)
    canvas.update()

varColor = tk.IntVar()  

button = tk.Button(master, text="Readme", command = callback)
button.grid(row=1, column=1, sticky=tk.W)

scale = tk.Scale(master, from_=1, to=99, orient=tk.HORIZONTAL, length=700, variable = varColor, command=change)
scale.grid(row=1, column=1, sticky=tk.E)

button = tk.Button(master, text="Readme", command = callback)
button.grid(row=1, column=3, sticky = tk.E)

master.mainloop()