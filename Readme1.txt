This program is to check the blindspot of the eye or angular vision

By increasing the Distance on the slider from the 0 to 400 you can get the lines further away one from another.
By increasing the Width on the 2nd slider from 0 to 200 you can modify the width so the values are visible

After setting up the distance, width and entering the value in the right box of your distance from screen to eyes in centimeter you can click Calculate and get AV

You already know how to get to the Readme button as long as you are reading this.

Usualy if you get a value between 0.5 and 1 you have a normal vision

Used python 3.6 with numpy, tkinter and webbrowser imported

Thank you
Darie Mihai-Cristian