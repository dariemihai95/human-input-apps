Human Hearing range: 20Hz-20.000Hz

The above values are for an healthy young adult.
The wider the range of your hearing, the better your sense of hearing is.

Set the volume from 0 to 100, default is 100
Set the durationin seconds(Do not expose more than 10 seconds to high level of sound)
Set the frequency range(20-20.000 Hz)

By clicking Play you can check if you hear the sound at certain frquencies or not
You have to click record every time you hear a sound.
After you try it with different frequencies you can click plot and see how your hearing
graph looks like.

Used python 3.6 with numpy, tkinter, pyaudio, matplotlib.pyplot and webbrowser imported

Thank you
Darie Mihai-Cristian